variable "redis_enabled" {
  type        = bool
  default     = false
  description = "Enable redis during installation"
}

variable "request_semver" {
  type        = string
  default     = "2.3.7"
  description = "Version of helm chart to use"
}

variable "threshhold_semvar" {
  type        = string
  default     = "3.0.0"
  description = "Helm chart where deviation started"
}

variable "redis_install_or_enable_label" {
  type        = list
  default     = ["enable", "install"]
  description = "Label used if requested chart is less-than or greater-than-equal to default version (enable is lt; install is gte;)"
}